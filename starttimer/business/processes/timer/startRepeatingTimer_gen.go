package timer

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	Start = "Start"

	SequenceFlow_0ci9n6x = "SequenceFlow_0ci9n6x"

	SequenceFlow_0gawtsr = "SequenceFlow_0gawtsr"

	SequenceFlow_1k9rpkj = "SequenceFlow_1k9rpkj"

	SequenceFlow_0eltyfl = "SequenceFlow_0eltyfl"

	SequenceFlow_1s44iah = "SequenceFlow_1s44iah"

	SequenceFlow_1t71n6w = "SequenceFlow_1t71n6w"

	ParallelGateway_08mjjnp = "ParallelGateway_08mjjnp"

	ParallelGateway_0en7ysa = "ParallelGateway_0en7ysa"

	SleepScriptTask = "SleepScriptTask"

	PrintScriptTask = "PrintScriptTask"

	EndEvent = "EndEvent"
)

func NewStartTimerProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, timerInput TimerInput) *dynamap.ProcessInstance {
	impl := StartTimerProcess{sync.RWMutex{}, &timerInput}
	pi := dynamap.ProcessInstance{Key: "StartTimerProcess", Id: id, BpmnEngine: bpmnEngine, Status: dynamap.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type StartTimerProcess struct {
	mu         sync.RWMutex
	timerInput *TimerInput
}

func (p *StartTimerProcess) ProcessInstanceData() any {
	p.mu.Lock()
	defer p.mu.Unlock()
	return *p.timerInput
}

func UnmarshalTask(r io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{Tasks: make([]dynamap.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *dynamap.BPMNEngine, key string) T {
	states, err := engine.StateStore.ReadElementStates(dynamap.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*dynamap.UserTask[T]).Data
}

func (p *StartTimerProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case Start:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0ci9n6x, id)

	case SequenceFlow_0ci9n6x:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0ci9n6x}
		outSeqKeys := []string{SequenceFlow_0gawtsr, SequenceFlow_1k9rpkj}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ParallelGateway_0en7ysa, id, SequenceFlow_0ci9n6x, inSeqKeys, outSeqKeys)

	case SequenceFlow_0gawtsr:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			fmt.Printf("[%s] Script executed at: %s\n", id, time.Now())
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, PrintScriptTask, id, script)

	case SequenceFlow_1k9rpkj:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			time.Sleep(100 * time.Millisecond)
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, SleepScriptTask, id, script)

	case SequenceFlow_0eltyfl:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0eltyfl, SequenceFlow_1t71n6w}
		outSeqKeys := []string{SequenceFlow_1s44iah}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ParallelGateway_08mjjnp, id, SequenceFlow_0eltyfl, inSeqKeys, outSeqKeys)

	case SequenceFlow_1s44iah:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, EndEvent, id, true)

	case SequenceFlow_1t71n6w:
		id := g.GenerateId(dynamap.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0eltyfl, SequenceFlow_1t71n6w}
		outSeqKeys := []string{SequenceFlow_1s44iah}
		return dynamap.NewParallelGateway(t.ProcessInstance.Id, t.Id, ParallelGateway_08mjjnp, id, SequenceFlow_1t71n6w, inSeqKeys, outSeqKeys)

	case ParallelGateway_08mjjnp:
		return nil

	case ParallelGateway_0en7ysa:
		return nil

	case SleepScriptTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_0eltyfl, id)

	case PrintScriptTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, SequenceFlow_1t71n6w, id)

	case EndEvent:
		return nil

	}
	return nil
}

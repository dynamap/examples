package start

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"starttimer/business/processes/timer"

	"gitlab.com/dynamap/dynamap"
)

type ProcessInstanceStartRequest struct {
	Id   string
	Data timer.TimerInput
}

type StartStartTimerProcessHandler struct {
	Engine *dynamap.BPMNEngine
}

func (s StartStartTimerProcessHandler) StartProcess(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	var startRequest ProcessInstanceStartRequest
	err := json.NewDecoder(req.Body).Decode(&startRequest)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}

	startEvent := dynamap.NewNoneStartEvent(startRequest.Id, "", timer.Start, s.Engine.GenerateId(dynamap.NoneStartEventType))
	p := timer.NewStartTimerProcess(startRequest.Id, s.Engine, startEvent, startRequest.Data)
	s.Engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	return nil
}

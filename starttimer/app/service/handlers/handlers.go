package handlers

import (
	"starttimer/app/service/handlers/complete"
	"starttimer/app/service/handlers/start"

	"gitlab.com/dynamap/dynamap"
	"gitlab.com/dynamap/web/app/services/process-api/handlers"
)

func GetProcessHandlers(engine *dynamap.BPMNEngine) []handlers.ProcessHandler {
	processHandlers := make([]handlers.ProcessHandler, 0)
	processHandlers = append(processHandlers, handlers.ProcessHandler{
		Engine:              engine,
		ProcessKey:          "StartTimerProcess",
		StartHandler:        start.StartStartTimerProcessHandler{Engine: engine}.StartProcess,
		CompleteTaskHandler: complete.CompleteStartTimerProcessTaskHandler{Engine: engine}.CompleteTask})
	return processHandlers
}

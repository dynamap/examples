package starters

import (
	"gitlab.com/dynamap/dynamap"
)

func GetStarters(engine *dynamap.BPMNEngine) []dynamap.StartEventProcessor {
	starters := make([]dynamap.StartEventProcessor, 0)

	starters = append(starters, StartStarter{engine})

	return starters
}

package starters

import (
	"context"
	"starttimer/business/processes/timer"
	"time"

	"gitlab.com/dynamap/dynamap"
)

type StartStarter struct {
	BpmnEngine *dynamap.BPMNEngine
}

func (t StartStarter) StartEventProcesses(ctx context.Context) {
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(5000000 * time.Microsecond):
				pid := t.BpmnEngine.IdGenerator.GenerateId(dynamap.ProcessInstanceType)
				startEvent := dynamap.NewTimerStartEvent(pid, "", "Start", t.BpmnEngine.GenerateId(dynamap.TimerStartEventType))
				p := timer.NewStartTimerProcess(pid, t.BpmnEngine, startEvent, timer.TimerInput{})
				t.BpmnEngine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})

			}
		}
	}()
}

package startinvoiceprocess

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	StartInvoiceEvent = "StartInvoiceEvent"

	Flow_19yn8fg = "Flow_19yn8fg"

	Flow_1svkt3g = "Flow_1svkt3g"

	Flow_02sonlo = "Flow_02sonlo"

	Flow_0rxw34n = "Flow_0rxw34n"

	Flow_1y93nl1 = "Flow_1y93nl1"

	Flow_1j6dnd4 = "Flow_1j6dnd4"

	Flow_01ywhcv = "Flow_01ywhcv"

	Flow_02jr12p = "Flow_02jr12p"

	ReviewInvoiceTask = "ReviewInvoiceTask"

	Gateway_07h7z8h = "Gateway_07h7z8h"

	ApprovedExclusiveGateway = "ApprovedExclusiveGateway"

	GenerateInvoiceScript = "GenerateInvoiceScript"

	NotifyApprovalScript = "NotifyApprovalScript"

	InvoiceApprovedEvent = "InvoiceApprovedEvent"

	InvoiceAutoApprovedEvent = "InvoiceAutoApprovedEvent"

	InvoiceRejectedEvent = "InvoiceRejectedEvent"
)

func NewStartInvoiceProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, data Data) *dynamap.ProcessInstance {
	impl := StartInvoiceProcess{sync.RWMutex{}, &data}
	pi := dynamap.ProcessInstance{Key: "StartInvoiceProcess", Id: id, BpmnEngine: bpmnEngine, Status: dynamap.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type StartInvoiceProcess struct {
	mu   sync.RWMutex
	data *Data
}

func (p *StartInvoiceProcess) ProcessInstanceData() any {
	p.mu.Lock()
	defer p.mu.Unlock()
	return *p.data
}

func UnmarshalTask(r io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{Tasks: make([]dynamap.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		case ReviewInvoiceTask:
			var taskData ApproveTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *dynamap.BPMNEngine, key string) T {
	states, err := engine.StateStore.ReadElementStates(dynamap.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*dynamap.UserTask[T]).Data
}

func (p *StartInvoiceProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case StartInvoiceEvent:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_19yn8fg, id)

	case Flow_19yn8fg:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			p.data.invoiceAmount = 550
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, GenerateInvoiceScript, id, script)

	case Flow_1svkt3g:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if p.data.invoiceAmount >= 500 {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_02sonlo, id)
			} else {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_1y93nl1, id)
			}
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, Gateway_07h7z8h, id, gwHandler)

	case Flow_02sonlo:
		id := g.GenerateId(dynamap.UserTaskType)
		return dynamap.NewUserTask[ApproveTask](t.ProcessInstance.Id, t.Id, ReviewInvoiceTask, id, true)

	case Flow_0rxw34n:
		id := g.GenerateId(dynamap.ExclusiveGatewayType)
		gwHandler := func() dynamap.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if TaskData[ApproveTask](engine, ReviewInvoiceTask).Approved {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_1j6dnd4, id)
			} else {
				id := g.GenerateId(dynamap.SequenceFlowType)
				return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_01ywhcv, id)
			}
		}
		return dynamap.NewExclusiveGateway(t.ProcessInstance.Id, t.Id, ApprovedExclusiveGateway, id, gwHandler)

	case Flow_1y93nl1:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, InvoiceAutoApprovedEvent, id, true)

	case Flow_1j6dnd4:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			fmt.Println("Invoice Approved")
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, NotifyApprovalScript, id, script)

	case Flow_01ywhcv:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, InvoiceRejectedEvent, id, true)

	case Flow_02jr12p:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, InvoiceApprovedEvent, id, true)

	case ReviewInvoiceTask:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_0rxw34n, id)

	case Gateway_07h7z8h:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case ApprovedExclusiveGateway:
		gw := currentElement.(*dynamap.ExclusiveGateway)
		return gw.NextElement

	case GenerateInvoiceScript:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_1svkt3g, id)

	case NotifyApprovalScript:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_02jr12p, id)

	case InvoiceApprovedEvent:
		return nil

	case InvoiceAutoApprovedEvent:
		return nil

	case InvoiceRejectedEvent:
		return nil

	}
	return nil
}

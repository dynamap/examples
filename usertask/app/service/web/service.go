package web

import (
	"fmt"
	"net/http"
	"usertask/app/service/handlers"
	"usertask/app/service/starters"

	"gitlab.com/dynamap/dynamap"
	processhandlers "gitlab.com/dynamap/web/app/services/process-api/handlers"
	"gitlab.com/dynamap/web/foundation/web"
)

type Service struct {
	Engine               *dynamap.BPMNEngine
	ProcessHandlers      []processhandlers.ProcessHandler
	StartEventProcessors []dynamap.StartEventProcessor
	Mux                  *web.App
	Address              string
}

func New() *Service {
	svc := Service{Engine: dynamap.NewMemoryBPMNEngine()}

	// Start process web handlers
	svc.ProcessHandlers = handlers.GetProcessHandlers(svc.Engine)

	// Start event processors
	svc.StartEventProcessors = starters.GetStarters(svc.Engine)

	// API Mux
	cfg := processhandlers.APIMuxConfig{Engine: svc.Engine, ProcessHandlers: svc.ProcessHandlers}
	svc.Mux = processhandlers.APIMux(cfg)
	svc.Address = "localhost:8081"
	return &svc
}

func (svc *Service) Run() error {
	for i := range svc.StartEventProcessors {
		svc.Engine.AddStartEventProcesser(svc.StartEventProcessors[i])
	}
	err := svc.Engine.Start()
	if err != nil {
		return err
	}

	fmt.Printf("Listening on http://%s\n", svc.Address)
	return http.ListenAndServe(svc.Address, svc.Mux)
}

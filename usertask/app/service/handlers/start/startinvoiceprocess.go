package start

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"usertask/business/processes/startinvoiceprocess"

	"gitlab.com/dynamap/dynamap"
)

type ProcessInstanceStartRequest struct {
	Id   string
	Data startinvoiceprocess.Data
}

type StartStartInvoiceProcessHandler struct {
	Engine *dynamap.BPMNEngine
}

func (s StartStartInvoiceProcessHandler) StartProcess(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	var startRequest ProcessInstanceStartRequest
	err := json.NewDecoder(req.Body).Decode(&startRequest)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}

	startEvent := dynamap.NewNoneStartEvent(startRequest.Id, "", startinvoiceprocess.StartInvoiceEvent, startRequest.Id)
	p := startinvoiceprocess.NewStartInvoiceProcess(startRequest.Id, s.Engine, startEvent, startRequest.Data)
	s.Engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	return nil
}

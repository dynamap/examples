package handlers

import (
	"usertask/app/service/handlers/complete"
	"usertask/app/service/handlers/start"

	"gitlab.com/dynamap/dynamap"
	"gitlab.com/dynamap/web/app/services/process-api/handlers"
)

func GetProcessHandlers(engine *dynamap.BPMNEngine) []handlers.ProcessHandler {
	processHandlers := make([]handlers.ProcessHandler, 0)
	processHandlers = append(processHandlers, handlers.ProcessHandler{
		Engine:              engine,
		ProcessKey:          "StartInvoiceProcess",
		StartHandler:        start.StartStartInvoiceProcessHandler{Engine: engine}.StartProcess,
		CompleteTaskHandler: complete.CompleteStartInvoiceProcessTaskHandler{Engine: engine}.CompleteTask})
	return processHandlers
}

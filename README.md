# DynaMap Examples
This project shows examples of DynaMap™ processes.
Each example has its own readme with more details.

- Helloworld: Uses a script task to print Hello [name], where [name] is passed in as a process variable.
- StartTimer: Executes script tasks at a repeating interval using a start timer event.
- UserTask: Shows how to define custom statically typed data for a user task and complete it.

## License
Apache License, Version 2.0
package helloworld

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/dynamap/dynamap"
)

const (
	StartHelloWorldEvent = "StartHelloWorldEvent"

	Flow_1a30jmi = "Flow_1a30jmi"

	Flow_1m2d1kw = "Flow_1m2d1kw"

	HelloWorldScript = "HelloWorldScript"

	EndHelloWorldEvent = "EndHelloWorldEvent"
)

func NewHelloWorldProcess(id string, bpmnEngine *dynamap.BPMNEngine, startElement dynamap.ElementStateProvider, input Input) *dynamap.ProcessInstance {
	impl := HelloWorldProcess{sync.RWMutex{}, &input}
	pi := dynamap.ProcessInstance{Key: "HelloWorldProcess", Id: id, BpmnEngine: bpmnEngine, Status: dynamap.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type HelloWorldProcess struct {
	mu    sync.RWMutex
	input *Input
}

func (p *HelloWorldProcess) ProcessInstanceData() any {
	p.mu.Lock()
	defer p.mu.Unlock()
	return *p.input
}

func UnmarshalTask(r io.Reader) (dynamap.CompleteUserTasksCmd, error) {
	tasksCmd := dynamap.CompleteUserTasksCmd{Tasks: make([]dynamap.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := dynamap.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		default:
			var taskData dynamap.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *dynamap.BPMNEngine, key string) T {
	states, err := engine.StateStore.ReadElementStates(dynamap.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*dynamap.UserTask[T]).Data
}

func (p *HelloWorldProcess) GetNextElement(engine *dynamap.BPMNEngine, g dynamap.IdGenerator, t *dynamap.Token, currentElement dynamap.ElementStateProvider) dynamap.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case StartHelloWorldEvent:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_1a30jmi, id)

	case Flow_1a30jmi:
		id := g.GenerateId(dynamap.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *dynamap.BPMNEngine, token *dynamap.Token, task *dynamap.ScriptTask) {
			fmt.Printf("Hello %s\n", p.input.Name)
		}
		return dynamap.NewScriptTask(t.ProcessInstance.Id, t.Id, HelloWorldScript, id, script)

	case Flow_1m2d1kw:
		id := g.GenerateId(dynamap.NoneEndEventType)
		return dynamap.NewNoneEndEvent(t.ProcessInstance.Id, t.Id, EndHelloWorldEvent, id, true)

	case HelloWorldScript:
		id := g.GenerateId(dynamap.SequenceFlowType)
		return dynamap.NewSequenceFlow(t.ProcessInstance.Id, t.Id, Flow_1m2d1kw, id)

	case EndHelloWorldEvent:
		return nil

	}
	return nil
}

package main

import (
	"helloworld/app/service/web"
	"log"
)

//go:generate strmsrv -input=../.. -output=../.. init
func main() {
	svc := web.New()
	err := svc.Run()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		svc.Engine.Wait()
		svc.Engine.Stop()
	}()
}

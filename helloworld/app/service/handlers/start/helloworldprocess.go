package start

import (
	"context"
	"encoding/json"
	"fmt"
	"helloworld/business/processes/helloworld"
	"net/http"

	"gitlab.com/dynamap/dynamap"
)

type ProcessInstanceStartRequest struct {
	Id   string
	Data helloworld.Input
}

type StartHelloWorldProcessHandler struct {
	Engine *dynamap.BPMNEngine
}

func (s StartHelloWorldProcessHandler) StartProcess(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	var startRequest ProcessInstanceStartRequest
	err := json.NewDecoder(req.Body).Decode(&startRequest)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}

	startEvent := dynamap.NewNoneStartEvent(startRequest.Id, "", helloworld.StartHelloWorldEvent, s.Engine.GenerateId(dynamap.NoneStartEventType))
	p := helloworld.NewHelloWorldProcess(startRequest.Id, s.Engine, startEvent, startRequest.Data)
	s.Engine.SendCommand(dynamap.StartProcessInstanceCmd{Instance: p})
	return nil
}

package complete

import (
	"context"
	"encoding/json"
	"fmt"
	"helloworld/business/processes/helloworld"
	"net/http"

	"gitlab.com/dynamap/dynamap"
)

type CompleteHelloWorldProcessTaskHandler struct {
	Engine *dynamap.BPMNEngine
}

func (h CompleteHelloWorldProcessTaskHandler) CompleteTask(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	completeReq, err := helloworld.UnmarshalTask(req.Body)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return nil
	}
	result, err := h.Engine.SendCommand(completeReq)
	if err != nil || result == nil {
		http.Error(w, "Error completing task", 500)
	}
	var resp dynamap.CompleteTasksResp
	resp = result.(dynamap.CompleteTasksResp)
	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		http.Error(w, "Error marshalling response", 500)
		fmt.Println(err)
	}
	return nil
}

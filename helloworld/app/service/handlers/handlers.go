package handlers

import (
	"helloworld/app/service/handlers/complete"
	"helloworld/app/service/handlers/start"

	"gitlab.com/dynamap/dynamap"
	"gitlab.com/dynamap/web/app/services/process-api/handlers"
)

func GetProcessHandlers(engine *dynamap.BPMNEngine) []handlers.ProcessHandler {
	processHandlers := make([]handlers.ProcessHandler, 0)
	processHandlers = append(processHandlers, handlers.ProcessHandler{
		Engine:              engine,
		ProcessKey:          "HelloWorldProcess",
		StartHandler:        start.StartHelloWorldProcessHandler{Engine: engine}.StartProcess,
		CompleteTaskHandler: complete.CompleteHelloWorldProcessTaskHandler{Engine: engine}.CompleteTask})
	return processHandlers
}
